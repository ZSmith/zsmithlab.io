---
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_projectm-backstory
title: Reminiscing on Project M
# post specific
# if not specified, .name will be used from _data/owner/[language].yml
author: Zach
# multiple category is not supported
category: programming
# multiple tag entries are possible
tags: [programming]
# thumbnail image for post
img: "../img/posts/project-m.png"
# disable comments on this page
#comments_disable: true

# publish date
date: 2022-12-01

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-02-10 08:11:06 +0900
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

Today marks the anniversary of the shutdown of the Project M game, a mod for Super Smash Brothers: Brawl that
earned fan (and media) praise. I am proud to have been one of the contributors to the game's development many years ago.
In this post I'll briefly mention some of the history of the game, and some of the work I put into it.

## Introduction

Super Smash Brothers is a game franchise released by
Nintendo. The original SSB was released on the
Nintendo 64, and since then has been followed by
releases on the Gamecube, Wii, and Switch.

Super Smash Brothers Melee was hailed by many fans
as an excellent game for playing competitively, and
over 15 years later continues to be played in tournaments.
Unfortunately the next iteration of the series,
Super Smash Brothers Brawl (for the Wii),
contained many features intended
to prevent competitive play. An increased amount in
random factors meant that individual skill had a smaller impact
in game outcome.

However, a series of modification tools for the Nintendo
Wii were released. The "USB Gecko" was an adaptor which
allowed connecting the console to a PC. From here, a
developer could debug the console, being able to inspect the
RAM and registers. A custom language, called "Ocarina",
was used to make "codes", which could affect gameplay
by writing to the RAM either once or at frequent intervals.

The work of several prominent hackers such as Phantom
Wings and brkirch meant that the capabilities of modders
rapidly grew. Instead of writing in Ocarina, hackers 
developed techniques to instead inject raw assembly code 
into Brawl. Later on, techniques were developed to load
modified files from the SD card instead of the Wii,
allowing for significant changes to core game assets.
Finally, a series of exploits such as Letterbomb and
Smash Stack allowed regular users to run code developed
by the modding community.

[Smashboards](https://smashboards.com){:target="_blank"}
is the home of
the largest online community for Super Smash Brothers,
and was at the time owned by Major League Gaming.
Although the board's adminstrators were broadly supportive
of the modding community, they were somewhat cautious of
its potential legal applications. As such, they created a
separate forum, the Smash Workshop, for creation and
discussion of custom content for Brawl. I was appointed
as the first moderator for the Smash Workshop, and had
weekly discussions with an MLG representative to discuss
the legal and competitive implications of what was being
worked on by the community.

## Technological Developments

The development of mods was very contingent on what the
hackers at the time were interested in, as the barrier
for entry (coding in PowerPC assembler code, having
custom hardware for a RAM debugger and the willingness
to use it) was somewhat high. As someone just geting into
programming, it was a great opportunity for me to find
ways to contribute. I made some contributions:

- At one point, modders were limited in the amount of
custom code that could be run. Multiple codes
had very similar structures, involving navigating over
what we called the "Root Pointer Tree", which allowed a
coder to access all of the most important locations in RAM
(character data and state for the players in game, and so
on). I wrote a code which merged several others together,
significantly decreasing the overall length.

- The hackers spunit262 and Phantom Wings created a
series of codes that allowed the modification of animation
and hitbox data for characters - describing how fast
attacks were and how much damage they would do. I wrote
some utilities to allow someone to read out this information,
making easier to make changes.

- We eventually ran into framerate lag problems from the
number of hitbox and animation changes we had in place!
These codes would loop through a list of every necessary 
change every frame for every animated character on the screen.
I wrote an indexed sequential search algorithm in
assembly, which ran the indexing at initial game load, significantly
improving the execution speed of these mods.

- Later on, as the ability to load files from the SD card
improved, many of the previous codes we had created became
obsolete. Instead of editing the data as it was loaded into (or read from)
RAM, we could write the changes into a custom character
file and load that file. Eventually, this lead to the
creation of completely custom characters. I was the first
to write and release a "Conditional File Loader" code, which
allowed players to select if and when to load alternate
character data. My code was later (significantly) 
improved upon by the coder Dantarion.

## Brawl+ and Project M

During all of this, I worked as part of a development team
for a modification called Brawl+. The aim of Brawl+ was
to attempt to make Brawl into a more competitive game by
increasing the speed of play and addressing some balance 
problems. Ultimately, the project did not fare too well -
there was no clear leadership, meaning that individual
contributors had free reign to rebalance characters as
they saw fit, and in many cases this made things worse.
Further, the incremental addition of new ways we could change the game (global physics changes, then animation changes, then attack changes...) meant that characters each had to receive a new round of balance changes as each new modding feature came out.

The reactions to Brawl+ were mixed amongst the greater community.
When I would visit in-person events, I would often showcase Brawl+ to players within the UK, and we even managed to hold some very small mini-tournaments for it at some events.
However, while many people found the game fun to play as a side event, there was little interest in adoption.
First, installing the mod was a lot of effort for users given the Homebrew software at the time.
More importantly, though, the audience was unclear -- Brawl players were not overly interested in a game that was so different to the original experience, while Melee players didn't see a strong reason to swap to this new game (as it still lacked many of the advanced techniques and deep knowledge present in Melee).

As the hacking scene developed, one of the major
contributors to Brawl+, shanus, started work on a project
to faithfully reproduce the attributes of characters from
Brawl's predecessor, Super Smash Brothers Melee.
This project came to be known as Project M, and received
over 1M downloads after its release in 2011.
Unlike Brawl+, the game was much more marketable to Melee players -- although it stated it did not intend to be a perfect clone of Melee, it captured many of the advanced techniques such as Wavedashing and L-Cancelling that contribute to the high skill ceiling and smooth feeling of Melee.
Further, characters were now developed using a specialised tool made by Phantom Wings that allowed directly modifying the character's data file rather than the use of custom codes.
This allowed a higher level of detail, and characters that felt very fluid and satisfying.
Over time, more and more characters were developed into their "Project M" versions, eventually including completely custom ones not included in Brawl.

As one of the lead developers for Brawl+, I was
grandfathered in to the Project M team (called the "PMBR",
or "Project M Back Room"), and contributed occasionally
to the discussions.
Although much of the character customisation was moved to these data files, there were still many codes in use for global changes to the engine.
In these cases, they had to be ported to work on different versions of the game (most notably, different regions such as EU, US and Japan).

Sadly, I stepped off the team
quite early, as I had to prepare for my A Level exams,
and had to leave my developing equipment at home when
I left for university soon after. I was thanked in the
credits of Project M until the project was taken down in 2015.
I am still thanked in the credits of some other
related projects, such as "Balanced Brawl".