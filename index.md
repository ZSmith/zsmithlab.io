---
layout: home
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_home

# image for page specific usage
img: ":moose.jpg"
# publish date (used for seo)
# if not specified, site.time will be used.
#date: 2022-03-03 12:32:00 +0000

# for override items in _data/lang/[language].yml
#title: My title
#button_name: "My button"
# for override side_and_top_nav_buttons in _data/conf/main.yml
#icon: "fa fa-bath"

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-03-03 12:32:00 +0000
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
# don't forget that this is root index.html. If you disable this, there will be no index.html page to open
#published: false
---

This is the home page of Zach Smith.

Currently I work as a Security Architect for Zortify, an AI/NLP
startup in Luxembourg.
Usually, my work involves understanding the technical infrastructure in place at an organisation.
From there, we implement technical or policy improvements, or deploy new solutions to harden security.
I love this work because it involves maintaining a big-picture view while also getting to muck around in the swamps of complex systems.
I also assist with establishing technical agreements -- research partnerships, contracts with high performance computing institutes, and so on.

Before this, I completed my PhD at the University of Luxembourg.
My research focused on the application of Formal Methods to
cybersecurity. By creating mathematical models of real-world
communication protocols (such as RFID or Internet messaging),
we can analyse their security. I'm originally a mathematician by study,
and successfully fulfilled the pure-math meme of justifying my work with "it has applications in cryptography". More on
[research](/tabs/research.html).

I have taken part in several programming projects in the
past, some of which ended up being quite successful. I'm a big
fan of game design as a way for honing my programming - 
you get to wear many hats, pick up artistic skills and have fun with the result!
I mostly work in modifying or extending existing games - either by
using supported modding tools or using more creative methods.
These days in what spare time I have I work with a friend developing a game in Unreal... but it'll be a while before there's anything worth showing. More on
[programming](/tabs/programming.html).

I am trying my best to write things about the other things that fill my life.
I think we are at our most interesting when we have a diverse set of hobbies -- I dance swing, speak Chinese, and play a card game with people I met in a boardgames design group in my late teens with a dozen active players.
You can see it in the [blog](/tabs/blog/).

If you want to contact me, you can email me using zach (at) (this website).
