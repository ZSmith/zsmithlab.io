---
title: Teaching
show_sidebar: True
layout: default
---

I have been active in teaching of various forms for
some time. In 2015 I spent a year working at the
[Beacon Hill Academy](/pages/teaching/beaconhill) in
Seongnam, Korea as a private teacher. After starting
at the [University of Luxembourg](/pages/teaching/university)
for my PhD, I lead Bachelors and Masters level
classes in several different courses.